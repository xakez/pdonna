$(function() {
  bindHandlers($(document));
});

function bindHandlers($el) {
  $el.find('.js-basket-item-delete').click(function(event) {
    $.get(this.href, function(data) {
      updateBasketFull($(data));
    });

    event.preventDefault();
  });

  (function() {
    var waitingAjax = false, needSubmit = false;

    $el.find('.basket-table__number-product').each(function() {
      var $form = $(this).closest('form'),
          $input = $(this).find('.js-basket-item-count'),
          $left = $(this).find('.num-arr-b'),
          $right = $(this).find('.num-arr-t');

      function sanitizeInput() {
        var oldVal = parseInt($input.val()),
            newVal = oldVal;

        if (!isFinite(oldVal) || oldVal < 1) {
          newVal = 1;
        }

        if (newVal !== oldVal) {
          $input.val(newVal);
        }
      }

      function getInputVal() {
        var val = parseInt($input.val());

        if (!isFinite(val)) {
          val = 1;
        }

        return val;
      }

      $input.on('keyup keydown input change', function(event) {
        if (event.type === 'keyup' || event.type === 'keydown') {
          if (event.keyCode !== 46 && event.keyCode !== 8 && event.keyCode !== 37 && event.keyCode !== 39) {
            if (event.keyCode == 38) {
              var value = parseInt($(this).val());
              $(this).val(value+1).change();
            }

            if (event.keyCode == 40) {
              var value = parseInt($(this).val());
              if (value > 1) $(this).val(value-1).change();
            }

            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
              event.preventDefault();
            }
          }
        }

        sanitizeInput();

        if (!waitingAjax) {
          waitingAjax = true;

          $.get($form.attr('action'), $form.serialize(), function(data) {
            updateBasketPrices($(data));

            waitingAjax = false;

            if (needSubmit) {
              needSubmit = false;
              $input.change();
            }
          });
        } else {
          needSubmit = true;
        }
      });

      $left.click(function() {
        $input.val(getInputVal() - 1);
        $input.change();
      });

      $right.click(function() {
        $input.val(getInputVal() + 1);
        $input.change();
      });
    });
  })();
}

function updateBasketPrices($page) {
  var updateItems, i;

  updateItems = [
  '.js-basket-total'
  ];

  for (i = 0; i < updateItems.length; i++) {
    $(updateItems[i]).html($page.find(updateItems[i]).html());
  }

  $page.find('.js-basket-item').each(function() {
    $('#' + $(this).attr('id')).find('.js-basket-item-total').html($(this).find('.js-basket-item-total').html());
  });
}

function updateBasketTable($page) {
  bindHandlers($('.js-basket').html($page.find('.js-basket').html()));
}

function updateBasketFull($page) {
  updateBasketPrices($page);
  updateBasketTable($page);
}
