$(document).ready(function(){

$(window).scroll(function() {
    var block_btn = $(".fix-cart");
    var offset = block_btn.offset();
    if(offset.top <= 160){
        $("#addcart").css('display','none');
    }
    else {
        $("#addcart").css('display','block');
    }
});

	$("a.thickbox").fancybox({
		prevEffect	: 'none',
		nextEffect	: 'none'
	});

  /* --- ������� --- */
  $('.main-slider').bxSlider({
    controls: true,
    pager: false,
    auto: true,
    autoControls: false,
    pause: 6000
  });

  /* --- ������� ������� --- */
  $('.contacts-slider').bxSlider({
    slideWidth: 220,
    maxSlides: 3,
    slideMargin: 10,
    /* moveSlides: 2, */
    controls: true,
    pager: false
  });

  /* --- ���� --- */
  $('.js-tab-list .js-tab:first').addClass ('active');
  $('.js-tab-content-list .js-tab-content:first').css ('display', 'block');
  $('.js-tab-list').delegate('.js-tab:not(.active)', 'click', function() {
    $(this).addClass('active').siblings().removeClass('active')
      .parents('.js-tab-wrapper').find('.js-tab-content-list .js-tab-content').hide()
      .eq($(this).index()).fadeIn('slow', function() {
         $(this).find('input, select.js-select').trigger('refresh');
       });
  });

  /* --- Form Styler --- */
  $('.js-styler').styler({selectSearch : false});
  BX.addCustomEvent('onAjaxSuccess', function() {
    $('.js-styler').styler({selectSearch : false});
  });
  /* --- �������� ����� --- */
  $('.js-feedback').click(function(event) {
    $('#fb-modal').arcticmodal();

    event.preventDefault();
  });


  /* --- ����������� �� �������� --- */
  $('.js-subscribe').click(function(event) {
    $('.subscribe-form').animate({
      opacity: 'toggle',
      width: 'toggle'
    });

    event.preventDefault();
  });

  if (location.hash === '#subscribe') {
    $('.js-subscribe').click();
  }

  /* --- ���� --- */
  $('.js-login').click(function(event) {
    var $modal = $('#login-modal'),
        $form = $modal.find('form');

    $modal.arcticmodal({
      afterOpen: function() {
        focusFirstNotEmpty($form);
      }
    });

    event.preventDefault();
  });

  /* --- ����������� --- */
  $('.js-register').click(function(event) {
    $('#register-modal').arcticmodal();

    event.preventDefault();
  });

  $('.js-toggle__trigger').click(function(event) {
    $(this).closest('.js-toggle').toggleClass('toggle_active');

    event.preventDefault();
  });

  $('.js-register-form').submit(function(event) {
    this['REGISTER[LOGIN]'].value = this['REGISTER[EMAIL]'].value;
  });

  $('.rating__radio').click(function(event) {
    var $input = $(this),
        $rating = $(this).closest('.rating');

    $.post($rating.attr('action'), $rating.serialize(), function(page) {
      var sel = '#' + $rating.attr('id'),
          $curRating = $(sel),
          $newRating = $(page).find(sel);

      $curRating.find('.rating__filled').replaceWith($newRating.find('.rating__filled'));
    });

    event.preventDefault();
  });

  (function() {
    var $images = $('.js-detail-pics__pic'),
        $bigImage = $('.js-detail-pics__big'),
        activeIndex = 0;

    $('.js-detail-pics__slider').bxSlider({
      pager: false,
      infiniteLoop: true,
      onSlideAfter : function() {
        
      }
    });
    
    $('.js-detail-pics__big').elevateZoom(
        { 
          zoomType : "inner",
          cursor: "crosshair",
          gallery:'js-detail-pics__slider li',
          galleryActiveClass: 'active'
        }
    );
    $bigImage.on('load', function() {
      $bigImage.fadeIn();
    });
    
    $images.first().addClass('active').addBack().click(function() {
     var $newImage = $(this);

        $images.removeClass('active');
        $newImage.addClass('active');
        

    });
    
  })();

  $('.catalog-size-list input').click(function() {
    updateSizeInput();
  });
  /*
  $(".js-detail-pics__big").elevateZoom({
    cursor: 'move',
    galleryActiveClass: 'active',
    zoomType: "inner",
    imageCrossfade: true,
    zoomWindowWidth: 380,
    zoomWindowHeight: 570,
    easing: false
  });
  */

  updateSizeInput();

  $('.catalog-colors-list input').click(function() {
    updateColorInput();
  });

  updateColorInput();
  
  //��������
  var fittingObject = new Fitting();
  var favoriteObject = new Favorite();
});

function updateSizeInput() {
  var $current = $('.catalog-size-list input:checked');

  $('.catalog-size-list input').not($current).closest('li').removeClass('active');
  $current.closest('li').addClass('active');
}

function updateColorInput() {
  var $current = $('.catalog-colors-list input:checked');

  $('.catalog-colors-list input').not($current).closest('li').removeClass('active');
  $current.closest('li').addClass('active');
}

function focusFirstNotEmpty($form) {
  var i, l;

  for (i = 0, l = $form[0].length; i < l; i++) {
    if (!$form[0][i].value) {
      $form[0][i].focus();
      break;
    }
  }
}



		var myShopPoints;

		myShopPoints = new Array();
		myShopPoints[0]= {
			myHeader: '������� "PRIMA DONNA"',
			myBody: '�. ����������������, ���������� ��������, �. 4<br> <a href="/contacts/elektrozavodskaya.php">���������� � ������</a>',
			myFooter: '+7 (968) 883-09-87, +7 (495) 963-90-69<br>10:00-20:00 (���������)',
			myPoint: {
				lat:'55.78317',
				lng:'37.708509'
			}
		}
		
		myShopPoints[1]= {
			myHeader: '������� "������ ����"',
			myBody: '�. ������, ��. ���������������� �.1, �� "XL", 2-� ����<br> <a href="/contacts/vdhx.php">���������� � ������</a>',
			myFooter: '+7 (926) 353-90-24<br>10:00-22:00 (���������)',
			myPoint: {
				lat:'55.891698',
				lng:'37.748619'
			}
		}
		
		myShopPoints[2]= {
			myHeader: '������� "PRIMA DONNA"',
			myBody: '�. ��������, ��-�. ������, �. 8<br> <a href="/contacts/balashiha.php">���������� � ������</a>',
			myFooter: '+7 (925) 306-21-58<br>10:00-20:00 (���������)',
			myPoint: {
				lat:'55.796663',
				lng:'37.933438'
			}
		}
		
		myShopPoints[3]= {
			myHeader: '������� "������ ����"',
			myBody: '�. ���������������, ��������� �����, 5,<br> <a href="/contacts/jeleznodorojniy.php">���������� � ������</a>',
			myFooter: '+7 (925) 306-21-57<br>10:00-20:00 (���������)',
			myPoint: {
				lat:'55.751381',
				lng:'38.010361'
			}
		}
		
		myShopPoints[4]= {
			myHeader: '������� "PRIMA DONNA"',
			myBody: '�. ������������, ��������� �����, 36�, �� "��������"<br> <a href="/contacts/elektrostal.php">���������� � ������</a>',
			myFooter: '+7 (926) 677-94-47<br>10:00-20:00 (���������)',
			myPoint: {
				lat:'55.812529',
				lng:'38.427098'
			}
		}

		myShopPoints[5]= {
			myHeader: '������� "PRIMA DONNA"',
			myBody: '�. ��������, ��-�. ������, �. 81<br> <a href="/contacts/balashikha-2.php">���������� � ������</a>',
			myFooter: '+7 (925) XXX-XX-XX<br>10:00-20:00 (���������)',
			myPoint: {
				lat:'55.8008',
				lng:'37.9778'
			}
		}

		myShopPoints[6]= {
			myHeader: '������� "PRIMA DONNA"',
			myBody: '3-� �� ���������� �������� � ������,<br> ������� ���������, ����� ��������, 3<br> <a href="/contacts/shubi-voronej.php">���������� � ������</a>',
			myFooter: '+7 (925) XXX-XX-XX<br>10:00-20:00 (���������)',
			myPoint: {
				lat:'51.78749629',
				lng:'39.20430250'
			}
		}