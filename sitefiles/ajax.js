function Fitting() {
  var that = this;

  this.controls = {};
  this.controls.add = $('.js-fitting-control-add');
  this.controls.remove = $('.js-fitting-control-remove');
  this.controls.send = $('.js-fitting-control-send');
  
  this.containers = {}
  this.containers.counter = $('.js-fitting-count');
  this.containers.item = $('.js-fitting-element');
  this.containers.form = $('.js-fitting-form');
  this.ajaxURL = '/bitrix/templates/.default/ajax/fitting.php';
  
  this.sendFields ={};
  this.fields ={}
  
  this.fields.name = {
      name : '���',
      input : this.containers.form.find('input[name=fitting-name]'),
      required : true,
      value : ''
  }
  this.fields.phone = {
      name : '�������',
      input : this.containers.form.find('input[name=fitting-phone]'),
      required : true,
      value : ''
  }
  this.fields.email = {
      name : '����� ����������� �����',
      input : this.containers.form.find('input[name=fitting-email]'),
      required : false,
      value : ''
  }
  this.fields.size = {
      name : '������',
      input : this.containers.form.find('input[name=fitting-size]'),
      required : true,
      value : ''
  }
  this.fields.time = {
      name : '����� ��������',
      input : this.containers.form.find('input[name=fitting-time]'),
      required : false,
      value : ''
  }
  this.fields.capcha = {
      name : '��� ������ �� ��������������� ����������',
      input : this.containers.form.find('input[name=capcha-word]'),
      required : true,
      value : ''
  }
  this.controls.add.click($.proxy(function(e) {
    e.preventDefault();
     this.ID = this.controls.add.data('id');
     this.add();
  }, this));
  
  this.controls.remove.click(function(e) {
    that.remove($(this));
    e.preventDefault();
   });
  
  this.controls.send.click($.proxy(function(e) {
    e.preventDefault();
    this.send();
  }, this))

}
//���������� � �����������
Fitting.prototype.add = function() {
  $.getJSON(
      this.ajaxURL,
      {
        ACTION : 'ADD',
        ID : this.ID
      },
      $.proxy(function(data) {
        this.containers.counter.html(data.ITEMS_COUNT);
        this.controls.add.fadeOut('fast', $.proxy(function() {this.controls.add.remove()}, this))
        alert('����� �������� � �����������');
      }, this)
  )
}
Fitting.prototype.remove = function ($el) {
  this.ID = $el.data('id');
  $.getJSON(
      this.ajaxURL,
      {
        ACTION : 'REMOVE',
        ID : this.ID
      },
      $.proxy(function(data) {
        $el.parent().fadeOut('fast', function() {
          $el.remove();
        });
        this.containers.counter.html(data.ITEMS_COUNT);
      }, this)
  )
}

Fitting.prototype.send = function() {
  var errorString = '';
  for(var key in this.fields) {
    var val = this.fields[key].input.val();
    if((val=='' || val == undefined) && this.fields[key].required == true) {
      errorString += '���� ' + this.fields[key].name + ' �� ��������� \n';
    }
    else {
      this.sendFields[key] = {}
      this.sendFields[key].name = this.fields[key].name;
      this.sendFields[key].value = val;
    }
  }
  if(errorString != '') {
    alert(errorString);
  }
  else {
    $.getJSON(
        this.ajaxURL,
        {
          ACTION : 'SEND',
          CAPCHA_SID : $('#js-fitting-capcha-sid').val(),
          FIELDS : this.sendFields
        },
        $.proxy(function(data) {
          console.log(data);
          if(typeof data.CAPTCHA !== undefined) {
            $('.js-fitting-capcha-img').attr('src', '/bitrix/tools/captcha.php?captcha_sid=' + data.CAPTCHA);
            $('#fitting-capcha-sid').val(data.CAPTCHA);
            $('.js-fitting-capcha-word').val('');
          }
          if ( data.ERROR != undefined) {
            err = data.ERROR.join("\n");
            alert(err);
          }
          if(data.SUCCESS == 1) {
            $('.catalog-preview__item').remove();
            $('.js-fitting-form-container').remove();
            for(var key in this.fields) {
              this.fields[key].input.val('');
            }
            this.containers.counter.html('0');
            $('section.workarea').html('<p>� ����������� ��� �� ������ ������</p>');
            alert(data.MESSAGE);
            
          }
         console.log(data);
        }, this)
    )
  }

}


//���������
function Favorite() {
  var that = this;

  this.controls = {};
  this.controls.add = $('.js-favorite-control-add');
  this.controls.remove = $('.js-favorite-control-remove');
  
  this.containers = {}
  this.containers.counter = $('.js-favorite-count');
  this.containers.item = $('.js-favorite-element')
  this.ajaxURL = '/bitrix/templates/.default/ajax/favorite.php';
  
  this.controls.add.click($.proxy(function(e) {
      e.preventDefault();
     this.ID = this.controls.add.data('id');
     this.add();
  }, this));
  
  this.controls.remove.click(function(e) {
    that.remove($(this));
    e.preventDefault();
   });

}
//���������� � �����������
Favorite.prototype.add = function() {
  $.getJSON(
      this.ajaxURL,
      {
        ACTION : 'ADD',
        ID : this.ID
      },
      $.proxy(function(data) {
        this.containers.counter.html(data.ITEMS_COUNT);
        this.controls.add.fadeOut('fast', $.proxy(function() {this.controls.add.remove()}, this))
        alert('����� �������� � ���������');
      }, this)
  )
}
Favorite.prototype.remove = function ($el) {
  this.ID = $el.data('id');
  $.getJSON(
      this.ajaxURL,
      {
        ACTION : 'REMOVE',
        ID : this.ID
      },
      $.proxy(function(data) {
        $el.parent().fadeOut('fast', function() {
          $el.remove();
        });
        this.containers.counter.html(data.ITEMS_COUNT);
      }, this)
  )
}

$(function() {
//������
var ajaxFilterURL = '/bitrix/templates/.default/ajax/filter.php';
//��������� �������� action ��� ��������� �������:
$('.js-change-action').change(function() {
  var action = $(this).val();
  $form = $(this).closest('form');
  $form.attr('action', action);
  if(action!= '') {
    $form.find('input[type=submit]').removeAttr('disabled');
  }
  else {
    $form.find('input[type=submit]').attr('disabled', 'disabled');
    $form.find('select').each(function() {
      if(!$(this).is('.js-change-action')) {
        $(this).attr('disabled', 'disabled').html('').trigger('refresh');
      }
    })
  }
  var sectionID = $(this).find('option:selected').data('id');
  if(sectionID != '' && sectionID != undefined) {
    $.getJSON(
        ajaxFilterURL,
        {ACTION : 'FILTER', 'SECTION_ID' : sectionID},
        function(data) {

          for(var i in data.ITEMS) {
           var curItem = data.ITEMS[i];
           
             var $select = $form.find('select[name="filter[PROPERTY_'+curItem.CODE+']"]');
             if(curItem.DISABLED == 1) {
               $select.html('<option value="">'+curItem.NAME +'</option>').attr('disabled', 'disabled').trigger('refresh');
             }
             else {
               $select.html('');
               var stringHTML = '<option value="">'+curItem.NAME +'</option>';
               for(var key in curItem.VALUES) {
                 stringHTML += '<option value="'+key+'">'+curItem.VALUES[key].VALUE+'</option>';
               }
               $select.append(stringHTML).removeAttr('disabled').trigger('refresh');
             }
           };
          
        }
    );
  }
  else {
   $form.find('select').each(function() {
     if(!$(this).is('.js-change-action')) {
       $(this).attr('disabled', 'disabled').html('').trigger('refresh');
     }
   })
  }
})

//������ ����
$('.js-tab-list-filter .js-tab:first').addClass ('active');
$('.js-tab-content-list-filter .js-tab-content:first').css ('display', 'block');
$('.js-tab-list-filter').delegate('.js-tab:not(.active)', 'click', function() {
  //������������� �������� �������
/*  var sectionID = $(this).data('section');
  $.getJSON(
      ajaxFilterURL,
      {SECTION_ID : sectionID, ACTION : 'CHILD_SECTIONS'},
      function(data) {
        var $childSelect = $('#js-child_category-'+sectionID);
        $childSelect.html();
        $childSelect.trigger('refresh');
        for(i=0; i<data.length; i++) {
          $childSelect.append('<option value="'+data[i].SECTION_PAGE_URL+'">'+data[i].NAME+'</option>');
        }
        $childSelect.trigger('refresh');
      }
  )
  */
  $(this).addClass('active').siblings().removeClass('active')
    .parents('.js-tab-wrapper-filter').find('.js-tab-content-list .js-tab-content').hide()
    .eq($(this).index()).fadeIn('slow', function() {
       $(this).find('input, select.select').trigger('refresh');
     });
});
})


