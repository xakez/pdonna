﻿$(document).ready(function(){
   var hideTime = [];

   var ci = $('.catalog-preview__item');
   for (var i=0;i<ci.length;i++){
      ci.eq(i).attr("numi", i%4);
   }
   
   $('.catalog-preview__item').click(function(ev){
      ev.stopImmediatePropagation();
         var fp = $(this).find('.fast-preview');
         fp.addClass("blockdelete");
         clearInterval(hideTime[$(this).attr("id")]);
         $('.fast-preview:not(".blockdelete")').hide();
         fp.removeClass("blockdelete");
         /*switch ($(this).attr("numi")){
            case "0":
               fp.css({"margin-left":"240px"});
               break;
            case "1":
               fp.css({"margin-left":"180px"});
               break;
            case "2":
               fp.css({"margin-left":"-480px"});
               break;
            case "3":
               fp.css({"margin-left":"-540px"});
               break;
         }*/
         fp.show();
         return false;
      });
      
   $('.left-col-in-fast > img').click(function(){
      $('.left-col-in-fast > img').removeClass("active");
      $(this).addClass("active");
      $(this).closest('.fast-preview').find('.big-ph-in-pr > img').attr("src", $(this).attr("big"));
   });
   $('.catalog-preview__item').hover(function(){
         var fp = $(this).find('.fast-preview');
         fp.addClass("blockdelete");
         clearInterval(hideTime[$(this).attr("id")]);
         $('.fast-preview:not(".blockdelete")').hide();
         fp.removeClass("blockdelete");
         /*switch ($(this).attr("numi")){
            case "0":
               fp.css({"margin-left":"240px"});
               break;
            case "1":
               fp.css({"margin-left":"180px"});
               break;
            case "2":
               fp.css({"margin-left":"-480px"});
               break;
            case "3":
               fp.css({"margin-left":"-540px"});
               break;
         }*/
         fp.show();
      },
   function(){
      var self = this;
      hideTime[$(this).attr("id")]= setTimeout(function(){
         $(self).find('.fast-preview').hide();
      }, 200)

   });


   $(window).scroll(function(){
      hideShowBut();
   });

   hideShowBut();
   
   $('.button-top').click(function(){
      $(document).scrollTop(0);
//      $('body').animate({ scrollTop: 0 }, 1100);
   });
   
   $('.full-fast-button').click(function(){
      try{
         ifr.remove();  
      }catch(e){}
      try{
         wait.remove();
      }catch(e){}
      ifr = $('<iframe src="'+$(this).attr('adr')+'" class="fast-preview-ifr"></iframe>');
      wait = $('<div class="preload">Подождите...</div>');
      $('body').append(ifr);
      $('body').append(wait);
      ifr.click(function(ev){
         ev.stopImmediatePropagation();
      })
   });
   
   $('body').click(function(){
      try{
         ifr.remove();  
      }catch(e){}
      try{
         wait.remove();
      }catch(e){}
   });
   $('.but-to-close-wind').click(function(){
      window.parent.ifr.remove();
   });
});

var ifr = null;
var wait = null;

function setHeightIfr(h){
   var hd = $(window).height(),
      t = (hd-h)/2;
   if (t<40){
      t = 40;
      h = hd-80;
   }
   $('.preload').remove();
   ifr.stop().animate({"height":h, "top": t});
}

function hideShowBut(){
   var st = $('body').scrollTop()||$(window).scrollTop();
   if (st>150){
      $('.button-top').show();
   } else {
      $('.button-top').hide();
   }
}